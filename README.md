# Developer 👋 Brad Harris

Age: 45 🔭 I’m currently working React Native and JavaScript to enhance web and mobile features for practical usages.

## 📫 How to reach me: bradharris@harrismbrad.com

Fort Lauderdale, FL | 754-317-2588

### Project Manager Agile Flow

Scrum Master, QA Testing

#### Professional Blue Collar Experience

Floor & Shop Project Management / Engineering and Construction
Filed Installation and shop welder/welding
Steel and Aluminum structural Platform for Telecommunications.
Building Comms Ghosting Installs
